#encontrar el mayor de los tres números ingresados por teclado.

#input de los numeros
num1 = int(input("Ingrese el primer numero: "))
num2 = int(input("Ingrese el segundo numero: "))
num3 = int(input("Ingrese el tercer numero: "))

#Suponemos que el primero es el mayor 
numMayor = num1

if num2>num1: numMayor = num2
if num3>num2: numMayor = num3

print("El numero mas grande es: " + str(numMayor))