#Escenario:
#   1.- Toma cualquier número entero que no sea negativo y que no sea cero y asígnale el nombre c0.
#   2.- Si es par, evalúa un nuevo c0 como c0 ÷ 2.
#   3.- De lo contrario, si es impar, evalúe un nuevo c0 como 3 × c0 + 1.
#   4.- Si c0 ≠ 1, salta al punto 2.
#   Escribe un programa que lea un número natural y ejecute los pasos anteriores siempre que c0 sea diferente de 1. 
# También queremos que cuente los pasos necesarios para lograr el objetivo. Tu código también debe mostrar todos los valores intermedios de c0.

import time
c0=int(input("Ingrese un numero: "))
pasos=0

while c0!=1:
    if(c0%2==0):
        c0=int(c0/2)
        pasos+=1
        print(str(c0) + " ", end="")
    else:
        c0=int(3*c0+1)
        pasos+=1   
        print(str(c0)+ " ", end="")

print(f"\nPasos: {pasos}")