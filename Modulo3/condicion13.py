###Tu tarea es escribir un programa que lea la cantidad de bloques que tienen los constructores, 
# y generar la altura de la pirámide que se puede construir utilizando estos bloques de cuadrados.

bloques = int(input("Ingrese el número de bloques: "))
por_fila=1
utilizado=0
altura=0
while True:
    utilizado+=por_fila
    if utilizado>bloques: break

    altura+=1
    por_fila+=1
    
        
print("La altura de la pirámide:", altura)