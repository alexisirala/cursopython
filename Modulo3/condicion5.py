##Verificar si un año es bisiesto
año = int(input("Introduzca un año: "))
if(año>=1582):
    if(año%4==0):
        print(f"El {año} es bisiesto")
    elif (año%400!=0):
        print(f"El {año} es común")
    else: 
        if (año%100!=0):
            print(f"El {año} es bisiesto")
        print(f"El {año} es común")
else: 
    print("No cae dentro del período del calendario gregoriano")