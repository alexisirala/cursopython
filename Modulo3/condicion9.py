# break - ejemplo

print("La instrucción de ruptura:")
for i in range(1,6):
    if i == 3:
        break               ##Se observa que el breack lo que hace es salir de la condicional automaticamente 
    print("Dentro del ciclo.", i)
print("Fuera del ciclo.")

# continua - ejemplo

print("\nLa instrucción continue:")
for i in range(1,6):
    if i == 3:
        continue            ##Se observa que el continue lo que hace es saltar esa iteracion ya que no imprime al ser i==3
    print("Dentro del ciclo.", i)
print("Fuera del ciclo.")